<?php
header('Content-Type: text/html; charset=UTF-8');
session_start();
if (!empty($_SESSION['login'])) {
    session_destroy();
    if ($_COOKIE['admin'] == '1') {
        setcookie('admin', '0');
        header('Location: admin.php');
    } else {
        header('Location: index.php');
    }
}
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    if (!empty($_SESSION['error_session'])) {
        $msg = $_SESSION['error_session'];
        print("<div>'$msg'</div>");
        $_SESSION['error_session'] = "";
    }
    ?>
    <form action="login.php" method="POST">
        <input name="login"/>
        <input name="password"/>
        <input type="submit" value="Войти"/>
    </form>
    <?php
} else {
    $user = 'u23768';
    $password = '4433413';
    $db = new PDO('mysql:host=localhost;dbname=u23768', $user, $password, array(PDO::ATTR_PERSISTENT => true));
    if ($_POST['login'] != "" && $_POST['password'] != "") {
        $login = $_POST['login'];
        $pass = $_POST['password'];
        $sth = $db->prepare("SELECT id, login, pass FROM form");
        $sth->execute();
        $r = $db->query("SELECT COUNT(*) FROM form");
        $count = $r->fetchColumn();
        $flag = 0;
        $data = $sth->fetchAll();
        for ($i = 0; $i < $count; $i++) {
            if (data[$i]['login'] == $login) {
                $flag = 1;
                if (password_verify(password_hash($pass, PASSWORD_DEFAULT), $data[$i]['pass'])) {
                    $_SESSION['error_session'] = "";
                    $id = $data[$i]['id'];
                    break;
                } else {
                    $_SESSION['error_session'] = "Неверный пароль!";
                    header('Location: login.php');
                    exit();
                }
            }
        }
    } else {
        $_SESSION['error_session'] = "Поля не заполнены!";
        header('Location: login.php');
        exit();
    }
    $_SESSION['login'] = $_POST['login'];
    $_SESSION['uid'] = $id;
    header('Location: index.php');
}
